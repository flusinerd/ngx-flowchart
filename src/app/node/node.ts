import { EmployeeGroup } from '../employee-group';

export class Node {
  productionStepId: string;
  label: string;
  employeeGroup: EmployeeGroup;
  left: number;
  top: number;
}
