import { Component, Input, AfterViewInit, OnInit, ViewChild, ElementRef, Output, EventEmitter, HostListener } from '@angular/core';
import { Node } from './node';
import { EmployeeGroup } from '../employee-group';
import { Vector2 } from '../vector2';
import { BehaviorSubject, Subject } from 'rxjs';
import { TouchSequence } from 'selenium-webdriver';
import { createLinkEvent } from './createLinkEvent';
import { $ } from 'protractor';

@Component({
  selector: 'app-node',
  templateUrl: './node.component.html',
  styleUrls: ['./node.component.scss']
})
export class NodeComponent implements OnInit {
  public timeoutHandler;
  public containerOffset: Vector2;
  public nodePosition: Vector2 = this.containerOffset;
  public lastMousePosition: Vector2;
  private offset: Vector2;
  private containerDimensions: Vector2;
  private panOffset: Vector2 = { x: 0, y: 0 };
  private _panStatus: boolean = false;
  public leftNodeSelected: boolean;
  public rightNodeSelected: boolean;
  public dragging: boolean = false;
  private clientX: number;
  private clientY: number;
  public hamburgerActive: boolean = false;
  private zoomLevel: number = 1;

  //Container Size
  @Input() container: any;
  @Input() offsetSubject: BehaviorSubject<Vector2>;
  @ViewChild('dragDummy') dragDummy: ElementRef;
  @Output() moving = new EventEmitter<boolean>();
  @Input() panStatus: BehaviorSubject<boolean>;
  @Input() node: Node;
  @Input() createdLinkSubject: Subject<boolean>;
  @Output() createLinkEvent = new EventEmitter<createLinkEvent>();
  @Input() zoomLevelSubject: BehaviorSubject<number>;

  @HostListener('document:dragover', ['$event'])
  ondragover(ev: MouseEvent) {
    ev.preventDefault();
    this.clientX = ev.clientX;
    this.clientY = ev.clientY;
  }


  constructor() {
  }

  ngOnInit() {
    this.containerOffset = { x: this.container.nativeElement.offsetLeft, y: this.container.nativeElement.offsetTop };
    this.containerDimensions = {
      x: this.container.nativeElement.offsetWidth,
      y: this.container.nativeElement.offsetHeight,
    }
    this.initPosition();
    this.offsetSubject.subscribe((pan: Vector2) => {
      this.panOffset = pan;
      this.panning();
    })

    //PanStatus
    this.panStatus.subscribe(status => this._panStatus = status);

    //Deselcting if link is created
    this.createdLinkSubject.subscribe((value) => {
      this.leftNodeSelected = false;
      this.rightNodeSelected = false;
    })

    this.zoomLevelSubject.subscribe(zoomLevel => this.zoomLevel = zoomLevel);
  }

  private panning() {
    this.positionNode();
  }

  private positionNode() {
    this.nodePosition = {
      x: this.node.left - this.panOffset.x,
      y: this.node.top - this.panOffset.y
    }
  }

  private initPosition() {
    this.positionNode();
  }

  private dragStart($event) {
    this.offset = {
      x: $event.clientX * 1 / this.zoomLevel - this.nodePosition.x,
      y: $event.clientY * 1 / this.zoomLevel - this.nodePosition.y,
    }
    //Remove Ghost
    this.moving.emit(true);
    $event.dataTransfer.setData('text', 'anything');
    $event.dataTransfer.setDragImage(this.dragDummy.nativeElement, 0, 0);

  }

  private drag($event: DragEvent, item) {
    let newPosition: Vector2 = {
      x: this.clientX * 1 / this.zoomLevel - this.offset.x,
      y: this.clientY * 1 / this.zoomLevel - this.offset.y,
    }
    this.node.left = newPosition.x + this.panOffset.x;
    this.node.top = newPosition.y + this.panOffset.y;
    this.positionNode();
  }

  private onDragEnd($event) {
    let newPosition: Vector2 = {
      x: this.clientX * 1 / this.zoomLevel - this.offset.x,
      y: this.clientY * 1 / this.zoomLevel - this.offset.y,
    }
    this.node.left = newPosition.x + this.panOffset.x;
    this.node.top = newPosition.y + this.panOffset.y;
    this.moving.emit(false);
    this.positionNode();
  }

  public mousedown($event) {
    this.moving.emit(true);
    this.dragging = true;
    this.clientX = $event.clientX;
    this.clientY = $event.clientY;

  }

  public mouseup() {
    this.dragging = false;
  }

  public createLink(isStart: boolean) {
    if (isStart) {
      this.rightNodeSelected = !this.rightNodeSelected;
      this.leftNodeSelected = false;
    } else if (!isStart) {
      this.leftNodeSelected = !this.leftNodeSelected;
      this.rightNodeSelected = false;
    }
    if (this.leftNodeSelected || this.rightNodeSelected) {
      let values: createLinkEvent = { node: this.node, isStart: isStart };
      this.createLinkEvent.emit(values);
    }
  }

  public closeHamburger() {
    this.hamburgerActive = false;
    this.moving.emit(false);
  }

  public openHamburger() {
    this.hamburgerActive = true;
    this.moving.emit(false);
  }
}
