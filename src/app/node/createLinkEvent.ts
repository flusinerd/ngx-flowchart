import { Node } from './node';

export class createLinkEvent {
  node: Node;
  isStart: boolean;
}