import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NodeComponent } from './node/node.component';
import { FlowchartComponent } from './flowchart/flowchart.component';
import { NgxGraphModule } from '@swimlane/ngx-graph';
import { LinkComponent } from './link/link.component';
import { MouseWheelDirective } from './flowchart/mouse-wheel.directive';
@NgModule({
  declarations: [
    AppComponent,
    NodeComponent,
    FlowchartComponent,
    LinkComponent,
    MouseWheelDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxGraphModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
