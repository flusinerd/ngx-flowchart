import { Vector2 } from '../vector2';
import { Component, ElementRef, ViewChild, AfterViewInit, EventEmitter, HostListener, OnInit } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { Node } from '../node/node';
import { EmployeeGroup } from '../employee-group';
import { createLinkEvent } from '../node/createLinkEvent';
import { Link } from './link';


@Component({
  selector: 'app-flowchart',
  templateUrl: './flowchart.component.html',
  styleUrls: ['./flowchart.component.scss']
})

export class FlowchartComponent implements OnInit {
  containerDimensions: Vector2;
  zoomExpression: string = "scale(1)"
  panOffset: Vector2 = { x: 0, y: 0 };
  mouseStartPosition: Vector2;
  holdTimeout;
  mouseHold = new EventEmitter();
  lastMousePosition: Vector2;
  lastMouseMovement: Vector2;
  holdingMouseDown: boolean;
  public offsetSubject = new BehaviorSubject<Vector2>({ x: 0, y: 0 })
  nodeMoving: boolean = false;
  public panStatus = new BehaviorSubject<boolean>(false);
  public employeeGroup: EmployeeGroup = { id: 1, name: 'EmployeeGroup' };
  public nodes: Node[] = [{ productionStepId: "1000", label: "First", employeeGroup: this.employeeGroup, left: 0, top: 0 }, { productionStepId: "1000", label: "Second", employeeGroup: this.employeeGroup, left: 200, top: 200 }];
  private linkingArray: createLinkEvent[] = [];
  public links: Link[] = []
  public createdLinkSubject = new Subject<boolean>();
  public movingSubject = new Subject<boolean>();
  private zoomLevel: number = 1;
  private zoomString: string;
  public zoomLevelSubject = new BehaviorSubject<number>(this.zoomLevel);
  private hovering: boolean = false;

  public height: number;
  public width: number;

  @ViewChild('flowchartContainer') elementView: ElementRef;

  ngOnInit() {
    this.height = 0.7 * window.innerHeight;
    this.width = 0.8 * window.innerWidth;
  }

  zoomOut() {
    if (this.hovering) {
      this.zoomLevel -= 0.05;
      if (this.zoomLevel <= 0.2) {
        this.zoomLevel = 0.2;
      }
      this.zoomString = `scale(${this.zoomLevel})`;
      this.zoomLevelSubject.next(this.zoomLevel);
    }
  }

  zoomIn() {
    if (this.hovering) {
      if (this.zoomLevel >= 1.4) {
        this.zoomLevel = 1.4;
      }
      else {
        this.zoomLevel += 0.05;
      }
      this.zoomString = `scale(${this.zoomLevel})`;
      this.zoomLevelSubject.next(this.zoomLevel);
    }
  }

  constructor() { }

  public mouseup($event: MouseEvent) {
    this.holdingMouseDown = false;
    this.panStatus.next(false);
  }

  public mousedown($event: MouseEvent) {
    if (!this.nodeMoving) {
      this.mouseStartPosition = {
        x: $event.clientX,
        y: $event.clientY
      }
      this.holdingMouseDown = true;
      this.panStatus.next(true);
      // this.mouseLoop($event);
      this.lastMousePosition = this.mouseStartPosition;
    }
  }

  @HostListener('mousemove', ['$event'])
  onMouseMove(event: MouseEvent) {
    if (this.lastMousePosition && this.holdingMouseDown && !this.nodeMoving) {
      this.panStatus.next(true);
      this.lastMouseMovement = {
        x: event.clientX - this.lastMousePosition.x,
        y: event.clientY - this.lastMousePosition.y,
      }
      this.lastMousePosition = { x: event.clientX, y: event.clientY };

      //Modify Offsets;
      this.panOffset = {
        x: this.panOffset.x += this.lastMouseMovement.x * 1 / this.zoomLevel,
        y: this.panOffset.y += this.lastMouseMovement.y * 1 / this.zoomLevel,
      }
      this.offsetSubject.next(this.panOffset);
    }
  }

  nodeMove($event) {
    if ($event === true) {
      this.nodeMoving = true;
      this.movingSubject.next(true);
    }
    else {
      this.movingSubject.next(false);
      this.nodeMoving = false;
    }
  }

  createLink($event: createLinkEvent) {
    this.linkingArray.unshift($event);
    if (this.linkingArray.length > 1) {
      //Create link
      let link: Link = { startNode: this.linkingArray[1].node, endNode: this.linkingArray[0].node };
      let i = this.checkIfLinkExists(link);
      if (i < 0) {
        this.links.push(link);
        this.createdLinkSubject.next(true);
      } else {
        this.links.splice(i, 1);
        this.createdLinkSubject.next(false);
      }
      this.linkingArray = [];
    }
  }

  checkIfLinkExists(checkLink: Link): number {
    for (let i = 0; i < this.links.length; i++) {
      if (this.links[i].startNode === checkLink.startNode && this.links[i].endNode === checkLink.endNode) {
        return i;
      }
    }
    return -1;
  }

  createNode() {
    this.nodes.push({ productionStepId: "200", label: "third", employeeGroup: this.employeeGroup, left: 300, top: 100 });
  }

  onMouseEnter() {
    this.hovering = true;
  }

  onMouseLeave() {
    this.hovering = false;
  }
}
