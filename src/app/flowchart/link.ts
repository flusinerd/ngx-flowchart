import { Node } from '../node/node';

export class Link {
  startNode: Node;
  endNode: Node;
}