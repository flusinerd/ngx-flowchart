export class EmployeeGroup {
  id: number;
  name: string;
  description?: string;
}
