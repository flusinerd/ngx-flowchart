import { Component, OnInit, Input } from '@angular/core';
import { Link } from '../flowchart/link';
import { Vector2 } from '../vector2';
import { BehaviorSubject } from 'rxjs';


@Component({
  selector: 'app-link',
  templateUrl: './link.component.html',
  styleUrls: ['./link.component.sass']
})
export class LinkComponent implements OnInit {

  private panOffset: Vector2
  private first: Vector2 = { x: 0, y: 0 };
  private second: Vector2 = { x: 0, y: 0 };
  private dimensions: Vector2 = { x: 0, y: 0 };
  private lineEnd: Vector2 = { x: 0, y: 0 };
  private lineStart: Vector2 = { x: 0, y: 0 };
  private svgPosition: Vector2 = { x: 0, y: 0 };
  private recalculateInterval;
  @Input() link: Link
  @Input() movingSubject: BehaviorSubject<boolean>;
  @Input() offsetSubject: BehaviorSubject<Vector2>;
  constructor() { }

  ngOnInit() {
    this.offsetSubject.subscribe((pan: Vector2) => {
      this.panOffset = pan;
      this.calculateLink();
    });
    this.calculateLink();

    this.movingSubject.subscribe((moving: boolean) => {
      if (moving) {
        if (!this.recalculateInterval) {
          this.recalculateInterval = setInterval(() => {
            this.calculateLink();
          }, 10);
        }
      }
      else {
        clearInterval(this.recalculateInterval);
        this.recalculateInterval = null;
      }
    })
  }

  private calculateLink() {
    this.first = {
      x: this.link.startNode.left + 240 - this.panOffset.x - 2,
      y: this.link.startNode.top + 50 - this.panOffset.y,
    }
    this.second = {
      x: this.link.endNode.left - this.panOffset.x + 2,
      y: this.link.endNode.top + 50 - this.panOffset.y,
    }

    //Check how to draw (Where each node is in respect to the other)

    if (this.first.x < this.second.x) {
      if (this.first.y <= this.second.y) {
        //Bottom Right 
        //First node x/y -> 2nd node x/y
        this.svgPosition = {
          x: this.first.x,
          y: this.first.y - 5,
        }
        this.lineStart = { x: 0, y: 5 };
        this.lineEnd = {
          x: this.second.x - this.first.x,
          y: this.second.y - this.first.y + 5,
        };
        this.dimensions = {
          x: this.lineEnd.x,
          y: this.lineEnd.y,
        }
      }

      if (this.first.y > this.second.y) {
        //Top Right
        this.svgPosition.x = this.first.x;
        this.svgPosition.y = this.second.y - 5;
        this.lineStart = { x: 0, y: this.first.y - this.second.y  + 5};
        this.lineEnd = { x: this.second.x - this.first.x, y: 5 }
        this.dimensions = {
          x: this.lineEnd.x,
          y: this.lineStart.y,
        }
      }
    } else {
      if (this.first.y > this.second.y) {
        //Top Left
        this.svgPosition.x = this.second.x;
        this.svgPosition.y = this.second.y - 5;
        this.lineStart.x = 0;
        this.lineStart.y = 5;
        this.lineEnd.x  = this.first.x - this.second.x;
        this.lineEnd.y =  this.first.y - this.second.y + 5;
        this.dimensions.x = this.lineEnd.x;
        this.dimensions.y = this.lineEnd.y;
      }
      else {
        //Bottom Left
        this.svgPosition.x = this.second.x;
        this.svgPosition.y = this.first.y - 5;
        this.lineStart.x = 0;
        this.lineStart.y = this.second.y - this.first.y + 5;
        this.lineEnd.x = this.first.x - this.second.x;
        this.lineEnd.y = 5;
        this.dimensions = {
          x: this.lineEnd.x,
          y: this.lineStart.y,
        }
      }
    } //else
    this.dimensions.y += 5;
  }

}
